package mainMethodTests;

import java.util.Scanner;

import spellingbee.server.SpellingBeeGame;

public class MainMethodTest {

	public static void main(String[] args) {
		
		int numFoundWords =0;
		
		System.out.println("Game start");
		
		while(numFoundWords != 1) {
		Scanner reader = new Scanner(System.in);
		SpellingBeeGame SBGame = new SpellingBeeGame("aeimnty");
		
		String attempt = "amity"; // reader.nextLine();
		int score=0;
		
		if(SBGame.getPossibleWords().contains(attempt)){
			
			int pointsForWord = SBGame.getPointsForWord(attempt);
			
			score = SBGame.getScore() + pointsForWord;
			
			String messageForWord = SBGame.getMessage(attempt);
			numFoundWords++;
		}
		else
		System.out.println("Does not contain");
	}
	}
}
