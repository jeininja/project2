package mainMethodTests;

import javafx.application.*;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import spellingbee.client.GameTab;
import spellingbee.network.Client;
import spellingbee.server.SpellingBeeGame;

public class SampleMain extends Application{
	private SpellingBeeGame game;
	Client client;
	public void start(Stage stage) {
		game = new SpellingBeeGame();
		
		Group root = new Group();
		
		TabPane tp = new TabPane();
		
		GameTab tab = new GameTab(client);
		
		tp.getTabs().add(tab);
		
		root.getChildren().add(tp);
		
		Scene scene = new Scene(root, 1000, 1000);
		scene.setFill(Color.PINK);
		
		stage.setTitle("Spelling Bee Game");
		stage.setScene(scene);
		
		stage.show();
	}
	
	public static void main(String[] args) {
		Application.launch(args);
	}
}
