package spellingbee.network;

import spellingbee.client.ISpellingBeeGame;
import spellingbee.server.SpellingBeeGame;

/**
 * This class will run on the server side and is used to connect the server code to the backend business code.
 * This class is where the "protocol" will be defined.
 */
public class ServerController {
	// This is the interface you will be creating!
	// Since we are storing the object as an interface, you can use any type of ISpellingBeeGame object.
	// This means you can work with either the SpellingBeeGame OR SimpleSpellingBeeGame objects and can
	// seamlessly change between the two.
	private ISpellingBeeGame spellingBee = new SpellingBeeGame();
	
	/**
	 * Action is the method where the protocol translation takes place.
	 * This method is called every single time that the client sends a request to the server.
	 * It takes as input a String which is the request coming from the client. 
	 * It then does some actions on the server (using the ISpellingBeeGame object)
	 * and returns a String representing the message to send to the client
	 * @param inputLine The String from the client
	 * @return The String to return to the client
	 */
	
	/**
	 * @author Alessandro Vendramin
	 * @author Jei wen Wu
	 * @param inputLine
	 * @return string depending on what the inputLine is
	 */
	public String action(String inputLine) {
		
		//If the inputLine starts with "getPoints", call getPointsForWord on the second word
		//returns a String representing the points that word gives
		if(inputLine.startsWith("getPoints")) {
			String[] request = inputLine.split(";");
			String input = request[1];
			String result = Integer.toString(spellingBee.getPointsForWord(input));
			//spellingBee.setScore(spellingBee.getScore(),spellingBee.getPointsForWord(input));
			return result;
		}
		
		//If the inputLine starts with "getMessage", call getMessage with the second word
		//returns a String message based on if the word is good or not and why
		if(inputLine.startsWith("getMessage")){
			String[] request = inputLine.split(";");
			String input = request[1];
			String result = spellingBee.getMessage(input);
			return result;
		}
		
		//If the inputLine starts with "getLetters", call getAllLetters
		//returns a String containing the 7 letters
		if(inputLine.startsWith("getLetters")){
			return spellingBee.getAllLetters();
		}
		
		//If the inputLine starts with "getCenterLetter", call getCenterLetter
		//returns a String containing the center letter of the 7 letters
		if(inputLine.startsWith("getCenterLetter")){
			String centerLetter = Character.toString(spellingBee.getCenterLetter());
			return centerLetter;
		}
		
		//if the inputLine starts with "getScore", call getScore
		//returns a String containing the total score
		if(inputLine.startsWith("getScore")){
			String result = Integer.toString((spellingBee.getScore()));
			return result;
		}
		
		
		/* Your code goes here!!!!
		 * Note: If you want to preserve information between method calls, then you MUST
		 * store it into private fields.
		 * You should use the spellingBee object to make various calls and here is where your communication
		 * code/protocol should go.
		 * For example, based on the samples in the assignment:
		 * if (inputLine.equals("getCenter")) {
		 * 	// client has requested getCenter. Call the getCenter method which returns a String of 7 letters
		 *      return spellingBee.getCenter();
		 * }
		 * else if ( ..... )*/
		return null;
	}
}
