
package spellingbee.server;

import java.util.HashSet;
import java.util.Set;

import spellingbee.client.ISpellingBeeGame;

/**
 * @author Alessandro Vendramin
 * @author Jei wen Wu
 * This class is the simplified/hardcoded version of SpellingBeeGame
 * 
 */
public class SimpleSpellingBeeGame implements ISpellingBeeGame{
	private String sevenLetters = "NMRAEIS";
	private int score;
	private String responseHolder;
	private String[] words = new String[5];
	
	
	public SimpleSpellingBeeGame() {
		this.words[0] = "NEMESIS";
		this.words[1] = "NAME";
		this.words[2] = "SANE";
		this.words[3] = "MANE";
		this.words[4] = "SAME";
		this.score = 0;
	}
	
	@Override
	public int[] getBrackets() {
		int[] array = new int[5];
		array[0] = 10;
		array[1] = 25;
		array[2] = 50;
		array[3] = 100;
		array[4] = 500;
		return array;
	}
	
	public String getResponseHolder() {
		return this.responseHolder;
	}
	
	@Override
	public int getPointsForWord(String attempt) {
		String[] attemptLetters = attempt.split("");
		int containsCenterLetter = attempt.indexOf(getCenterLetter(), 0);
		if(containsCenterLetter != -1) {
			if(attempt.length()==4) {
				return 1;
			}
			if(attempt.length()==5) {
				return 5;
			}
			if(attempt.length()==6) {
				return 6;
			}
			if(attempt.length()>=7) {
				int counter=0;
				Set<String> letterBucket = new HashSet<String>();
				for(int i=0; i<attemptLetters.length; i++) {
					if(!letterBucket.contains(attemptLetters[i])) {
						letterBucket.add(attemptLetters[i]);
						counter++;
					}
				}
				if(counter==7) {
					return attempt.length()+7;
				}
				return attempt.length();
			}
		}
		return 0;
	}
	
	@Override
	public String getMessage(String response) {
		String containsWord = "Correct";
		String notContainsWord = "Wrong";
		String alreadyContainsWord = "Already guessed";
		String missingCenterLetter = "Missing center letter";
		String shortWord = "Word too short";
		
		Set<String> letterBucket = new HashSet<String>();
		if(response.length() < 4) {
			return shortWord;
		}
		if(letterBucket.contains(response)) {
			return alreadyContainsWord;
		}
		
		int containsCenterLetter = response.indexOf(getCenterLetter());
		if(containsCenterLetter != -1) {
			for(int i = 0; i < words.length; i++) {
				if(response.equals(words[i])) {
					letterBucket.add(response);
					return containsWord;
				}
			}
			return notContainsWord;
		}
		else {
			return missingCenterLetter;
		}
	}
	
	@Override
	public String getAllLetters() {
		return this.sevenLetters;
	}
	
	@Override
	public char getCenterLetter() {
		return this.sevenLetters.charAt(4);
	}
	
	@Override
	public int getScore() {
		return this.score;
	}
}
