package spellingbee.server;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import spellingbee.client.ISpellingBeeGame;

/**
 * @author Alessandro Vendramin
 * @author Jei wen Wu
 * Creates the SpellingBeeGame and implements the ISpellingBeeGame interface
 * Overrides all the empty methods inside the interface
 * Sets up the game and has the getters for the informations about the game
 */
public class SpellingBeeGame implements ISpellingBeeGame{
	
	//Creating the global private variables
	private String letters = "";
	private char centerLetter;
	private int score = 0;
	private Set<String> foundWords = new HashSet<String>();
	
	//Reads the english.txt file and sets them all as possibleWords
	private static Set<String> possibleWords = createWordsFromFile("C:\\Users\\jei_w\\Documents\\Programming III\\project_2\\project\\english.txt");
	
	//Creating the random number generator ranges from 0-6
	Random RNG = new Random();
	int centerNum = RNG.nextInt(6);
	
	//Creating the Scanner to read files
	Scanner combinationReader;
	Scanner textFileReader;
	Scanner dictionaryReader;
	
	//Constructor with no input
	public SpellingBeeGame() {
		//Creates a file that points to letterCombination.txt
		File combinations = new File("C:\\Users\\jei_w\\Documents\\Programming III\\project_2\\project\\letterCombinations.txt");
		try {
			//Setting the Scanner so it reads the file
			combinationReader = new Scanner(combinations);
			//Counts the number of lines in the file
			int lineCount=0;
			while (combinationReader.hasNextLine()){
				lineCount++;
				combinationReader.nextLine();
			}
			
			//Generate a random number up to the number of lines in the file
		int combinationLineNumber = RNG.nextInt(lineCount);
		try {
			//Gets the content of the line chosen by the random number generator
			this.letters = Files.readAllLines(Paths.get("C:\\Users\\jei_w\\Documents\\Programming III\\project_2\\project\\letterCombinations.txt")).get(combinationLineNumber);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//Setting a random letter to be the center letter from the chosen line
		this.centerLetter = letters.charAt(centerNum);
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
			}
	}
	
	//Constructor with a string of 7 letters input
	public SpellingBeeGame(String sevenLetters) {
		
		//Splitting the input into individual letters and adding them to the letters 1 by 1
		if(sevenLetters.length()==7) {
		String[] holder = sevenLetters.split("");
		for(int i=0; i<holder.length; i++) {
			this.letters = this.letters+holder[i];
		}
		
		//Setting a random letter to be the center letter
		int centerNum = RNG.nextInt(6);
		this.centerLetter = letters.charAt(centerNum);
	}
		else {
			throw new IllegalArgumentException("Input parameter is not exactly 7 letters. Try again");
		}
	}
	
	//getter for how many points an attempt awards
@Override
public int getPointsForWord(String attempt) {
	String[] attemptLetters = attempt.split("");
	if(SpellingBeeGame.possibleWords.contains(attempt)) {
		int containsCenterLetter = attempt.indexOf(getCenterLetter(), 0);
		if(containsCenterLetter != -1) {
			if(!foundWords.contains(attempt)) {
				if(attempt.length()==4) {
					this.score = this.score+1;
				return 1;
			}
			if(attempt.length()==5) {
				this.score = this.score+5;
				return 5;
			}
			if(attempt.length()==6) {
				this.score = this.score+6;
				return 6;
			}
			if(attempt.length()>=7) {
				int counter=0;
				Set<String> letterBucket = new HashSet<String>();
				for(int i=0; i<attemptLetters.length; i++) {
					if(!letterBucket.contains(attemptLetters[i])) {
						letterBucket.add(attemptLetters[i]);
						counter++;
					}
				}
				if(counter==7) {
					this.score = this.score+14;
					return attempt.length()+7;
				}
				this.score = this.score+attempt.length();
				return attempt.length();
			}
			}
			return 0;
		}
		return 0;
	}
	return 0;
}

//getter for user message depending on the attempt
@Override
public String getMessage(String attempt) {
	String containsWord = "Correct";
	String notContainsWord = "Wrong";
	String alreadyContainsWord = "Already guessed";
	String missingCenterLetter = "Missing center letter";
	String shortWord = "Word too short";
	String welcomeMessage = "Welcome to the game";
	
	if(!attempt.equals("")){
		if(attempt.length()>=4) {
			if(!this.foundWords.contains(attempt)) {
				int containsCenterLetter = attempt.indexOf(getCenterLetter());
				if(containsCenterLetter != -1) {
					if(SpellingBeeGame.possibleWords.contains(attempt)) {
						this.foundWords.add(attempt);
						return containsWord;
					}
					else {
						return notContainsWord;
					}
				}
				return missingCenterLetter;
			}
			return alreadyContainsWord;
		}
		return shortWord;
	}
	return welcomeMessage;
}

//getter for the 7 available letters
@Override
public String getAllLetters() {
	return this.letters;
}

//getter for the center letter
@Override
public char getCenterLetter() {
	return this.centerLetter;
}

//getter for the total score
@Override
public int getScore() {
	return this.score;
}

//getter for all the possible words in the English dictionary
public Set<String> getPossibleWords(){
	return SpellingBeeGame.possibleWords;
}

//getter to set up the bracket points depending on how many points the 7 letters can make in total
//splitting the total points into brackets
@Override
public int[] getBrackets() {
	
	String[] sevenLetters = this.getAllLetters().split("");
	int[] checkPoints = new int[5];
	int maxPoints=0;
	
	try {
		File dictionary = new File("C:\\Users\\jei_w\\Documents\\Programming III\\project_2\\project\\english.txt");
		dictionaryReader = new Scanner(dictionary);
	
	while(dictionaryReader.hasNextLine()) {
		String word = dictionaryReader.nextLine();
		String[] wordSplit = word.split("");
		int counter=0;
		
		if(wordSplit.length>=4) {
			for(int i = 0; i<wordSplit.length; i++) {
				for(int j=0; j<sevenLetters.length; j++) {
					if(wordSplit[i].equals(sevenLetters[j])) {
						counter++;
					}
				}
			}
		}
		if(counter == wordSplit.length) {
			maxPoints = maxPoints + getPointsForWord(word);
		}
	}
	}
	catch (FileNotFoundException e) {
		e.printStackTrace();
	}
	
	int firstCheckPoint=(int) Math.round(maxPoints*0.25);
	int secondCheckPoint=(int) Math.round(maxPoints*0.5);
	int thirdCheckPoint=(int) Math.round(maxPoints*0.75);
	int fourthCheckPoint= (int) Math.round(maxPoints*0.9);
	int fifthCheckPoint=maxPoints;
	
	checkPoints[0]=firstCheckPoint;
	checkPoints[1]=secondCheckPoint;
	checkPoints[2]=thirdCheckPoint;
	checkPoints[3]=fourthCheckPoint;
	checkPoints[4]=fifthCheckPoint;
	
	return checkPoints;
}

//method to create a Set containing all the possible words in a given path to a file
public static Set<String> createWordsFromFile(String path) {
	Set<String> returnSet = new HashSet<String>();
	try {
		Scanner textFileReader = new Scanner (new File(path));
		
		while (textFileReader.hasNext()) {
			String word = textFileReader.nextLine();
			returnSet.add(word.toLowerCase());
		}
		textFileReader.close();
	}
	catch (FileNotFoundException e){
		e.printStackTrace();
	}
	return returnSet;
}
}
