package spellingbee.client;

import java.io.FileNotFoundException;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import spellingbee.network.Client;
import spellingbee.server.SimpleSpellingBeeGame;
import spellingbee.server.SpellingBeeGame;
import javafx.scene.paint.*;
import javafx.beans.value.*;

/**
 * @author Alessandro Vendramin
 * @author Jei wen Wu
 * This class extends Application to be able to create e GUI
 * Creates a client and a ISpellingBeeGame and passes the client to the game
 * Starts the game
 */
public class SpellingBeeClient extends Application{
	
	Client client = new Client();
	ISpellingBeeGame game = new SpellingBeeGame();
	
	public void start(Stage stage) throws FileNotFoundException {
		
		//Creating the root
		Group root = new Group();
		VBox vbox = new VBox();
		
		//Creating the containers
		TabPane tp = new TabPane();
		
		//Creating the tab elements which have their own specific elements inside
		GameTab gameTab = new GameTab(client);
		ScoreTab scoreTab = new ScoreTab(client, game);
		
		//Getting the tabs inside the bigger container Tab
		tp.getTabs().add(gameTab);
		tp.getTabs().add(scoreTab);
		
		//Adding the containers to the root page
		vbox.getChildren().addAll(tp);
		root.getChildren().addAll(vbox);
		
		//Adding an event listener to the scoreField TextField inside the gameTab tab
		//When the text of that textField changes, call refresh
		gameTab.getScoreField().textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue ) {
				scoreTab.refresh();
			}
		});

		//Setting the size and color of the window scene
		Scene scene = new Scene (root, 400, 400);
		scene.setFill(Color.PINK);
		
		//Setting the title of the window
		stage.setTitle("Spelling Bee Game");
		
		//Setting the scene on the window
		stage.setScene(scene);
		
		//Display the window
		stage.show();
	}

	public static void main(String[] args) {
	//Launch the application
    Application.launch(args);
	}
	
}