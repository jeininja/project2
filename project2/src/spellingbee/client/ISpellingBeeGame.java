package spellingbee.client;

import java.io.FileNotFoundException;

/**
 * @author Alessandro Vendramin
 * @author Jei wen Wu
 * This interface regroups the required methods that classes who implements it needs to have
 */
public interface ISpellingBeeGame {

	//This message should return the number of points that a given word is worth according to the Spelling Bee rules
	public int getPointsForWord(String attempt);
	
	/*This method should check if the word attempt is a valid word or not according to the Spelling Bee rules
	It should return a message based on the reason it is rejected or a positive message (e.g. �good� or �great�) if it is a valid word*/
	public String getMessage(String attempt);
	
	//This method should return the set of 7 letters (as a String) that the spelling bee object is storing
	public String getAllLetters();
	
	//This method should return the center character. That is, the character that is required to be part of every word.
	public char getCenterLetter();
	
	//This method should return the current score of the user.
	public int getScore();
	
	//This method will be used in the gui to determine the various point categories.
	public int[] getBrackets() throws FileNotFoundException;

}
