package spellingbee.client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import spellingbee.client.gui.ButtonChoice;
import spellingbee.network.Client;

/**
 * @author Alessandro Vendramin
 * @author Jei wen Wu
 * This class builds the GUI for the game tab
 * This is the tab where the client should play on
 */
public class GameTab extends Tab{
	
	Client client;
	TextField totalPoints;
	
	public GameTab(Client client) {
		super("Game");
		
		this.client = new Client();
		
		//Creating the containers
		VBox overall = new VBox();
		HBox buttons = new HBox();
		HBox responseField = new HBox();
		HBox submitLine = new HBox();
		HBox scoreLine = new HBox();
		
		//Creating the elements
		TextField response = new TextField();
		Button submit = new Button("Submit");
		TextField status = new TextField();
		totalPoints = new TextField();
		
		//Creating the 7 button elements
		String word = client.sendAndWaitMessage("getLetters");
		String[] letters = word.split("");
		Button letter1 = new Button(letters[0]);
		Button letter2 = new Button(letters[1]);
		Button letter3 = new Button(letters[2]);
		Button letter4 = new Button(letters[3]);
		Button letter5 = new Button(letters[4]);
		Button letter6 = new Button(letters[5]);
		Button letter7 = new Button(letters[6]);
		
		//storing the 7 buttons into a button array
		Button[] buttonArray = new Button[letters.length];
		buttonArray[0]=letter1;
		buttonArray[1]=letter2;
		buttonArray[2]=letter3;
		buttonArray[3]=letter4;
		buttonArray[4]=letter5;
		buttonArray[5]=letter6;
		buttonArray[6]=letter7;
		
		//Setting the center letter to red
		String center = client.sendAndWaitMessage("getCenterLetter");
		for(int i=0; i<letters.length;i++) {
			if(center.equals(letters[i])) {
				buttonArray[i].setStyle("-fx-text-fill: #ff0000");
			}
		}
		
		//Adding the elements to the containers
		responseField.getChildren().add(response);
		submitLine.getChildren().add(submit);
		scoreLine.getChildren().addAll(status, totalPoints);
		overall.getChildren().add(buttons);
		overall.getChildren().add(responseField);
		overall.getChildren().add(submitLine);
		overall.getChildren().add(scoreLine);
		buttons.getChildren().addAll(letter1, letter2, letter3, letter4, letter5, letter6, letter7);
		
		this.setContent(overall);
		
		//Changing the width of the TextField of the user input
		response.setPrefWidth(300);
		
		//Adding an event listener to each button
		ButtonChoice button1 = new ButtonChoice(response, letters[0]);
		letter1.setOnAction(button1);
		ButtonChoice button2 = new ButtonChoice(response, letters[1]);
		letter2.setOnAction(button2);
		ButtonChoice button3 = new ButtonChoice(response, letters[2]);
		letter3.setOnAction(button3);
		ButtonChoice button4 = new ButtonChoice(response, letters[3]);
		letter4.setOnAction(button4);
		ButtonChoice button5 = new ButtonChoice(response, letters[4]);
		letter5.setOnAction(button5);
		ButtonChoice button6 = new ButtonChoice(response, letters[5]);
		letter6.setOnAction(button6);
		ButtonChoice button7 = new ButtonChoice(response, letters[6]);
		letter7.setOnAction(button7);
		
		//Adding an event listener to the submit button inline
		submit.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				
				//This call updates the total score with the input word
				client.sendAndWaitMessage("getPoints;"+response.getText());
				
				//Getting the total score and setting it to the totalPoints textField
				int totalScore = Integer.parseInt(client.sendAndWaitMessage("getScore"));
				totalPoints.setText(Integer.toString(totalScore));
				
				//Gets the appropriate message depending on the input and sets it in the status textfield
				status.setText(client.sendAndWaitMessage("getMessage;"+response.getText()));
				
				//Resets the input textfield
				response.setText("");
			}
		});
	}
	
	//This method is used to enable the scoreTab to get the totalPoints
	public TextField getScoreField() {
		return totalPoints;
	}
}
