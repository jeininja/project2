package spellingbee.client;

import java.io.FileNotFoundException;

import javafx.scene.control.Tab;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import spellingbee.network.Client;

/**
 * @author Alessandro Vendramin
 * @author Jei wen Wu
 * This class extends the Tab class which means it is a Tab
 * Creates the score tab for the GUI
 */
public class ScoreTab extends Tab{
	
	Client client;
	private Text currentScorePoints = new Text("0");
	
	//Creating the elements (text)
	private Text queenBee;
	private Text genius;
	private Text amazing;
	private Text good;
	private Text gettingStarted;
	private Text currentScore;
	
	//Creating the elements (points)
	private Text OneHundredPercent;
	private Text NinetyPercent;
	private Text SeventyFivePercent;
	private Text FiftyPercent;
	private Text TwentyFivePercent;
	
	public ScoreTab(Client client, ISpellingBeeGame game) throws FileNotFoundException {
		
		super("Score");
		this.client = client;
		
		//Creating the container
		GridPane gp = new GridPane();
		
		//Setting the texts of the Text elements
		queenBee = new Text("Queen Bee");
		genius = new Text("Genius");
		amazing = new Text("Amazing");
		good = new Text("Good");
		gettingStarted = new Text("Getting Started");
		currentScore = new Text("Current Score");
		
		int [] bracketHolder = game.getBrackets();
		
		OneHundredPercent = new Text(Integer.toString(bracketHolder[4]));
		NinetyPercent = new Text(Integer.toString(bracketHolder[3]));
		SeventyFivePercent = new Text(Integer.toString(bracketHolder[2]));
		FiftyPercent = new Text(Integer.toString(bracketHolder[1]));
		TwentyFivePercent = new Text(Integer.toString(bracketHolder[0]));;
		
		//Adding the elements inside the container
		gp.add(queenBee, 0, 0, 1, 1);
		gp.add(genius, 0, 1, 1, 1);
		gp.add(amazing, 0, 2, 1, 1);
		gp.add(good, 0, 3, 1, 1);
		gp.add(gettingStarted, 0, 4, 1, 1);
		gp.add(currentScore, 0, 5, 1, 1);
		
		gp.add(OneHundredPercent, 1, 0, 1, 1);
		gp.add(NinetyPercent, 1, 1, 1, 1);
		gp.add(SeventyFivePercent, 1, 2, 1, 1);
		gp.add(FiftyPercent, 1, 3, 1, 1);
		gp.add(TwentyFivePercent, 1, 4, 1, 1);
		gp.add(currentScorePoints, 1, 5, 1, 1);
		
		//Giving the gridPane container gaps between each element
		gp.setHgap(50);
		gp.setVgap(10);
		
		this.setContent(gp);
		
		//Setting the color of each element to be a specific color at default
		gettingStarted.setFill(Color.GRAY);
		good.setFill(Color.GRAY);
		amazing.setFill(Color.GRAY);
		genius.setFill(Color.GRAY);
		queenBee.setFill(Color.GRAY);
		
		TwentyFivePercent.setFill(Color.RED);
		FiftyPercent.setFill(Color.RED);
		SeventyFivePercent.setFill(Color.RED);
		NinetyPercent.setFill(Color.RED);
		OneHundredPercent.setFill(Color.RED);
	}
	
	/**
	 * This method is called when there is a change in the scoreField in the GameTab tab
	 * Updates the total score of a game inside the currentScorePoints textfield
	 * Changes the color of the text depending on the percentage of total points a client has
	 */
	public void refresh() {
		
		//int points=Integer.parseInt(currentScorePoints.getText())+Integer.parseInt(client.sendAndWaitMessage("getScore"));
		currentScorePoints.setText(client.sendAndWaitMessage("getScore"));
		
		//Parsing the content of the textField into an int and storing it
		int currentPoints = Integer.parseInt(currentScorePoints.getText());
		
		//Storing the text from the bracket Text
		int twentyFivePercent = Integer.parseInt(TwentyFivePercent.getText());
		int fiftyPercent = Integer.parseInt(FiftyPercent.getText());
		int seventyFivePercent = Integer.parseInt(SeventyFivePercent.getText());
		int ninetyPercent = Integer.parseInt(NinetyPercent.getText());
		int oneHundredPercent = Integer.parseInt(OneHundredPercent.getText());

		//ScoreTab elements change color on currentPoints conditions
		if(currentPoints>=oneHundredPercent) {
			queenBee.setFill(Color.BLACK);
			
			OneHundredPercent.setFill(Color.GREEN);
		}
		if(currentPoints>=ninetyPercent) {
			genius.setFill(Color.BLACK);
			
			NinetyPercent.setFill(Color.GREEN);
		}
		if(currentPoints>=seventyFivePercent) {
			amazing.setFill(Color.BLACK);
			
			SeventyFivePercent.setFill(Color.GREEN);
		}
		if(currentPoints>=fiftyPercent) {
			good.setFill(Color.BLACK);
			
			FiftyPercent.setFill(Color.GREEN);
		}
		if(currentPoints>=twentyFivePercent){
			gettingStarted.setFill(Color.BLACK);
			
			TwentyFivePercent.setFill(Color.GREEN);
		}
	}
}
