package spellingbee.client.gui;

import javafx.scene.control.TextField;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * @author Alessandro Vendramin
 * @author Jei wen Wu
 * This class is called when a letter button is clicked on
 * It adds that specific letter to the input textfield
 */
public class ButtonChoice implements EventHandler<ActionEvent>{
	private TextField response;
	private String button;
	
	public ButtonChoice(TextField response, String button) {
		this.response = response;
		this.button = button;
	}
	
	@Override
	public void handle(ActionEvent event) {
		//If the response TextField is empty the first time the user clicks on any button
		//it will be set its text to empty and concatenate that button's letter
		if(response.getText() == null) {
			response.setText("");
		}
		response.setText(response.getText() + button);
	}
}
